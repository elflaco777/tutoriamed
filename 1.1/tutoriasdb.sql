-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-11-2015 a las 21:24:52
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tutoriasdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
  `idalumno` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `genero` char(1) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `idnacionalidad` int(11) DEFAULT NULL,
  `idciudad_origen` int(11) DEFAULT NULL,
  `idprovincia` int(11) DEFAULT NULL,
  `dni` bigint(10) DEFAULT NULL,
  `domicilio_local` varchar(45) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `piso` int(11) DEFAULT NULL,
  `depto` varchar(5) DEFAULT NULL,
  `localidad` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `idtiporesidencia` int(11) DEFAULT NULL,
  `conviviente` varchar(200) DEFAULT NULL,
  `sosten_economico` varchar(100) DEFAULT NULL,
  `sosten_familia` varchar(100) DEFAULT NULL,
  `edad_hijos` varchar(45) DEFAULT NULL,
  `idtrabajo` int(11) DEFAULT NULL,
  `idtitulo` int(11) DEFAULT NULL,
  `iddeporte` int(11) DEFAULT NULL,
  `idhorastrabajo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_disminucion`
--

CREATE TABLE IF NOT EXISTS `alumno_disminucion` (
  `id_alumno_disminucion` int(11) NOT NULL,
  `idalumno` int(11) DEFAULT NULL,
  `iddisminucion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_materia`
--

CREATE TABLE IF NOT EXISTS `alumno_materia` (
  `idalumno_materia` int(11) NOT NULL,
  `idmateria` int(11) DEFAULT NULL,
  `idalumno` int(11) DEFAULT NULL,
  `horaentrada` time DEFAULT NULL,
  `horasalida` time DEFAULT NULL,
  `dia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ayuda`
--

CREATE TABLE IF NOT EXISTS `ayuda` (
  `idayuda` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `idalumno` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE IF NOT EXISTS `ciudad` (
  `idciudad` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`idciudad`, `nombre`) VALUES
(1, 'mar del plata');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colegio`
--

CREATE TABLE IF NOT EXISTS `colegio` (
  `idcolegio` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursocolegio`
--

CREATE TABLE IF NOT EXISTS `cursocolegio` (
  `idcurso` int(11) NOT NULL,
  `idcolegio` int(11) DEFAULT NULL,
  `idalumno` int(11) DEFAULT NULL,
  `idciudad` int(11) DEFAULT NULL,
  `fechagraduacion` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deporte`
--

CREATE TABLE IF NOT EXISTS `deporte` (
  `iddeporte` int(11) NOT NULL,
  `decripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `deporte`
--

INSERT INTO `deporte` (`iddeporte`, `decripcion`) VALUES
(1, 'volleyball');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disminucion`
--

CREATE TABLE IF NOT EXISTS `disminucion` (
  `iddisminucion` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `franjahoraria`
--

CREATE TABLE IF NOT EXISTS `franjahoraria` (
  `idfranjaHoraria` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `franjahoraria_alumno`
--

CREATE TABLE IF NOT EXISTS `franjahoraria_alumno` (
  `idfranjaHoraria_alumno` int(11) NOT NULL,
  `idfranjaHoraria` int(11) DEFAULT NULL,
  `idalumno` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcion`
--

CREATE TABLE IF NOT EXISTS `funcion` (
  `idfuncion` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `funcion`
--

INSERT INTO `funcion` (`idfuncion`, `descripcion`) VALUES
(1, 'alta_alumno'),
(2, 'alta_alumno'),
(3, 'alta_fecha'),
(4, 'alta_deporte'),
(5, 'alta_deporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horastrabajo`
--

CREATE TABLE IF NOT EXISTS `horastrabajo` (
  `idhorastrabajo` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE IF NOT EXISTS `materia` (
  `idmateria` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE IF NOT EXISTS `mensaje` (
  `idmensaje` int(11) NOT NULL,
  `idautor` int(11) NOT NULL,
  `idreceptor` int(11) NOT NULL,
  `contenido` varchar(254) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL COMMENT 'diferencia entre mensaje y sugerencia de cartelera'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nacionalidad`
--

CREATE TABLE IF NOT EXISTS `nacionalidad` (
  `idnacionalidad` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='	';

--
-- Volcado de datos para la tabla `nacionalidad`
--

INSERT INTO `nacionalidad` (`idnacionalidad`, `descripcion`) VALUES
(3, 'Argentina'),
(4, 'Bolivia'),
(5, 'Colombia'),
(6, 'Peru');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observacion`
--

CREATE TABLE IF NOT EXISTS `observacion` (
  `idobservacion` int(11) NOT NULL,
  `contenido` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `idperfil` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`idperfil`, `descripcion`) VALUES
(1, 'Administrador'),
(3, 'Tutor'),
(4, 'Alumno'),
(5, 'Coordinadora');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_funcion`
--

CREATE TABLE IF NOT EXISTS `perfil_funcion` (
  `idperfil_funcion` int(11) NOT NULL,
  `idperfil` int(11) NOT NULL,
  `idfuncion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `idpersona` int(11) DEFAULT NULL,
  `nombre` int(11) DEFAULT NULL,
  `apellido` int(11) DEFAULT NULL,
  `legajo` int(11) DEFAULT NULL,
  `email` int(11) DEFAULT NULL,
  `hash` int(11) DEFAULT NULL,
  `sal` int(11) DEFAULT NULL,
  `idperfil` int(11) DEFAULT NULL,
  `activo` int(11) DEFAULT NULL,
  `descripcion` int(11) DEFAULT NULL,
  `idperfil_funcion` int(11) DEFAULT NULL,
  `idfuncion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `legajo` int(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `hash` varchar(200) DEFAULT NULL,
  `sal` varchar(32) DEFAULT NULL,
  `idperfil` int(11) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL,
  `dni` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `nombre`, `apellido`, `legajo`, `email`, `hash`, `sal`, `idperfil`, `activo`, `dni`) VALUES
(22, 'Mariano', 'Fernandez', 7777, 'fernando@gmail.com', 'cc14361e3a60f2a9c95a7397b6c3e20067b5b8353d196a5b60f17a5da9e514490484f88ee29fd4037a76d9e25412b0564946262cfb9f6316041054e9bfec7666', 'M£f\\c×½Ëôç6PìÕY9\rjÁªmWév', 3, 0, 0),
(23, 'Carlos', 'Sabena', 1111, 'carlos@gmail.com', '7e117d1a1c9a9e859370a3e0727f88d18b953f8acba8440d7772688bb6cd0a43a1440af760e3137c0a2460ea4511bcfe948482ed1ddfac76252372c6fcd64819', '<\nñ×''þßäÐ(«©Xãõ «öR¾åö®', 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE IF NOT EXISTS `provincia` (
  `idprovincia` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reunion`
--

CREATE TABLE IF NOT EXISTS `reunion` (
  `idreunion` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `idtutor` int(11) NOT NULL,
  `idobservacion` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoobservacion`
--

CREATE TABLE IF NOT EXISTS `tipoobservacion` (
  `idtipoObservacion` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoobservacion_observacion`
--

CREATE TABLE IF NOT EXISTS `tipoobservacion_observacion` (
  `idtipoObservacionobservacion` int(11) NOT NULL,
  `idtipoObservacion` int(11) NOT NULL,
  `idobservacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiporesidencia`
--

CREATE TABLE IF NOT EXISTS `tiporesidencia` (
  `idtiporesidencia` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `titulo`
--

CREATE TABLE IF NOT EXISTS `titulo` (
  `idtitulo` int(10) unsigned NOT NULL,
  `descripcion` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajo`
--

CREATE TABLE IF NOT EXISTS `trabajo` (
  `idtrabajo` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor`
--

CREATE TABLE IF NOT EXISTS `tutor` (
  `idtutor` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`idalumno`),
  ADD KEY `fk_alumno_persona1_idx` (`idpersona`),
  ADD KEY `fk_alumno_deporte1_idx` (`iddeporte`),
  ADD KEY `fk_alumno_nacionalidad1_idx` (`idnacionalidad`),
  ADD KEY `fk_alumno_ciudad1_idx` (`idciudad_origen`),
  ADD KEY `fk_alumno_titulo1_idx` (`idtitulo`),
  ADD KEY `fk_alumno_provincia1_idx` (`idprovincia`),
  ADD KEY `fk_alumno_trabajo1_idx` (`idtrabajo`),
  ADD KEY `fk_alumno_residencia1_idx` (`idtiporesidencia`),
  ADD KEY `fk_alumno_horasTrabajo1_idx` (`idhorastrabajo`);

--
-- Indices de la tabla `alumno_disminucion`
--
ALTER TABLE `alumno_disminucion`
  ADD PRIMARY KEY (`id_alumno_disminucion`),
  ADD KEY `fk_alumno_has_disminucion_disminucion1_idx` (`iddisminucion`),
  ADD KEY `fk_alumno_has_disminucion_alumno1_idx` (`idalumno`);

--
-- Indices de la tabla `alumno_materia`
--
ALTER TABLE `alumno_materia`
  ADD PRIMARY KEY (`idalumno_materia`),
  ADD KEY `fk_alumno_materia_materia1_idx` (`idmateria`),
  ADD KEY `fk_alumno_materia_alumno1_idx` (`idalumno`);

--
-- Indices de la tabla `ayuda`
--
ALTER TABLE `ayuda`
  ADD PRIMARY KEY (`idayuda`),
  ADD KEY `fk_ayuda_alumno1_idx` (`idalumno`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idciudad`);

--
-- Indices de la tabla `colegio`
--
ALTER TABLE `colegio`
  ADD PRIMARY KEY (`idcolegio`);

--
-- Indices de la tabla `cursocolegio`
--
ALTER TABLE `cursocolegio`
  ADD PRIMARY KEY (`idcurso`),
  ADD KEY `fk_cursocolegio_ciudad1_idx` (`idciudad`),
  ADD KEY `fk_cursocolegio_alumno1_idx` (`idalumno`),
  ADD KEY `fk_cursocolegio_colegio1_idx` (`idcolegio`);

--
-- Indices de la tabla `deporte`
--
ALTER TABLE `deporte`
  ADD PRIMARY KEY (`iddeporte`);

--
-- Indices de la tabla `disminucion`
--
ALTER TABLE `disminucion`
  ADD PRIMARY KEY (`iddisminucion`);

--
-- Indices de la tabla `franjahoraria`
--
ALTER TABLE `franjahoraria`
  ADD PRIMARY KEY (`idfranjaHoraria`);

--
-- Indices de la tabla `franjahoraria_alumno`
--
ALTER TABLE `franjahoraria_alumno`
  ADD PRIMARY KEY (`idfranjaHoraria_alumno`),
  ADD KEY `fk_franjaHoraria_has_alumno_alumno1_idx` (`idalumno`),
  ADD KEY `fk_franjaHoraria_has_alumno_franjaHoraria1_idx` (`idfranjaHoraria`);

--
-- Indices de la tabla `funcion`
--
ALTER TABLE `funcion`
  ADD PRIMARY KEY (`idfuncion`);

--
-- Indices de la tabla `horastrabajo`
--
ALTER TABLE `horastrabajo`
  ADD PRIMARY KEY (`idhorastrabajo`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`idmateria`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`idmensaje`),
  ADD KEY `fk_mensaje_persona1_idx` (`idautor`),
  ADD KEY `fk_mensaje_persona2_idx` (`idreceptor`);

--
-- Indices de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  ADD PRIMARY KEY (`idnacionalidad`);

--
-- Indices de la tabla `observacion`
--
ALTER TABLE `observacion`
  ADD PRIMARY KEY (`idobservacion`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`idperfil`);

--
-- Indices de la tabla `perfil_funcion`
--
ALTER TABLE `perfil_funcion`
  ADD PRIMARY KEY (`idperfil_funcion`),
  ADD KEY `fk_perfil_funcion_perfil1_idx` (`idperfil`),
  ADD KEY `fk_perfil_funcion_funcion1_idx` (`idfuncion`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`),
  ADD KEY `fk_persona_perfil1_idx` (`idperfil`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`idprovincia`);

--
-- Indices de la tabla `reunion`
--
ALTER TABLE `reunion`
  ADD PRIMARY KEY (`idreunion`),
  ADD KEY `fk_reunion_alumno1_idx` (`idalumno`),
  ADD KEY `fk_reunion_tutor1_idx` (`idtutor`),
  ADD KEY `fk_reunion_observacion1_idx` (`idobservacion`);

--
-- Indices de la tabla `tipoobservacion`
--
ALTER TABLE `tipoobservacion`
  ADD PRIMARY KEY (`idtipoObservacion`);

--
-- Indices de la tabla `tipoobservacion_observacion`
--
ALTER TABLE `tipoobservacion_observacion`
  ADD PRIMARY KEY (`idtipoObservacionobservacion`),
  ADD KEY `fk_tipoObservacion_has_observacion_observacion1_idx` (`idobservacion`),
  ADD KEY `fk_tipoObservacion_has_observacion_tipoObservacion1_idx` (`idtipoObservacion`);

--
-- Indices de la tabla `tiporesidencia`
--
ALTER TABLE `tiporesidencia`
  ADD PRIMARY KEY (`idtiporesidencia`);

--
-- Indices de la tabla `titulo`
--
ALTER TABLE `titulo`
  ADD PRIMARY KEY (`idtitulo`);

--
-- Indices de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD PRIMARY KEY (`idtrabajo`);

--
-- Indices de la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`idtutor`),
  ADD KEY `fk_tutor_persona_idx` (`idpersona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `idalumno` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `alumno_disminucion`
--
ALTER TABLE `alumno_disminucion`
  MODIFY `id_alumno_disminucion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `alumno_materia`
--
ALTER TABLE `alumno_materia`
  MODIFY `idalumno_materia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ayuda`
--
ALTER TABLE `ayuda`
  MODIFY `idayuda` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idciudad` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `colegio`
--
ALTER TABLE `colegio`
  MODIFY `idcolegio` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cursocolegio`
--
ALTER TABLE `cursocolegio`
  MODIFY `idcurso` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `deporte`
--
ALTER TABLE `deporte`
  MODIFY `iddeporte` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `disminucion`
--
ALTER TABLE `disminucion`
  MODIFY `iddisminucion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `franjahoraria`
--
ALTER TABLE `franjahoraria`
  MODIFY `idfranjaHoraria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `funcion`
--
ALTER TABLE `funcion`
  MODIFY `idfuncion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `horastrabajo`
--
ALTER TABLE `horastrabajo`
  MODIFY `idhorastrabajo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `idmateria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  MODIFY `idmensaje` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  MODIFY `idnacionalidad` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `observacion`
--
ALTER TABLE `observacion`
  MODIFY `idobservacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `idperfil` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `perfil_funcion`
--
ALTER TABLE `perfil_funcion`
  MODIFY `idperfil_funcion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idpersona` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `idprovincia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reunion`
--
ALTER TABLE `reunion`
  MODIFY `idreunion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipoobservacion`
--
ALTER TABLE `tipoobservacion`
  MODIFY `idtipoObservacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipoobservacion_observacion`
--
ALTER TABLE `tipoobservacion_observacion`
  MODIFY `idtipoObservacionobservacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tiporesidencia`
--
ALTER TABLE `tiporesidencia`
  MODIFY `idtiporesidencia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `titulo`
--
ALTER TABLE `titulo`
  MODIFY `idtitulo` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  MODIFY `idtrabajo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tutor`
--
ALTER TABLE `tutor`
  MODIFY `idtutor` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
