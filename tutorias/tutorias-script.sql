-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tutoriasdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tutoriasdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tutoriasdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `tutoriasdb` ;

-- -----------------------------------------------------
-- Table `tutoriasdb`.`perfil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`perfil` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`perfil` (
  `idperfil` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idperfil`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`persona`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`persona` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`persona` (
  `idpersona` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NULL COMMENT '',
  `apellido` VARCHAR(45) NULL COMMENT '',
  `legajo` INT NULL COMMENT '',
  `email` VARCHAR(45) NULL COMMENT '',
  `hash` VARCHAR(200) NULL COMMENT '',
  `sal` VARCHAR(32) NULL COMMENT '',
  `idperfil` INT NULL COMMENT '',
  `activo` TINYINT(1) NOT NULL COMMENT '',
  PRIMARY KEY (`idpersona`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_persona_perfil1_idx` ON `tutoriasdb`.`persona` (`idperfil` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`tutor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`tutor` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`tutor` (
  `idtutor` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idpersona` INT NOT NULL COMMENT '',
  PRIMARY KEY (`idtutor`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_tutor_persona_idx` ON `tutoriasdb`.`tutor` (`idpersona` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`deporte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`deporte` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`deporte` (
  `iddeporte` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `decripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`iddeporte`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`nacionalidad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`nacionalidad` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`nacionalidad` (
  `idnacionalidad` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idnacionalidad`)  COMMENT '')
ENGINE = InnoDB
COMMENT = '	';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`ciudad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`ciudad` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`ciudad` (
  `idciudad` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idciudad`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`titulo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`titulo` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`titulo` (
  `idtitulo` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(80) NULL COMMENT '',
  PRIMARY KEY (`idtitulo`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`provincia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`provincia` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`provincia` (
  `idprovincia` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idprovincia`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`trabajo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`trabajo` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`trabajo` (
  `idtrabajo` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idtrabajo`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`tiporesidencia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`tiporesidencia` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`tiporesidencia` (
  `idtiporesidencia` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idtiporesidencia`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`horasTrabajo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`horasTrabajo` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`horasTrabajo` (
  `idhorastrabajo` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idhorastrabajo`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`alumno`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`alumno` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`alumno` (
  `idalumno` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idpersona` INT NOT NULL COMMENT '',
  `genero` CHAR NULL COMMENT '',
  `fecha_nacimiento` DATE NULL COMMENT '',
  `idnacionalidad` INT NULL COMMENT '',
  `idciudad_origen` INT NULL COMMENT '',
  `idprovincia` INT NULL COMMENT '',
  `dni` BIGINT(10) NULL COMMENT '',
  `domicilio_local` VARCHAR(45) NULL COMMENT '',
  `numero` VARCHAR(45) NULL COMMENT '',
  `piso` INT NULL COMMENT '',
  `depto` VARCHAR(5) NULL COMMENT '',
  `localidad` VARCHAR(45) NULL COMMENT '',
  `telefono` VARCHAR(45) NULL COMMENT '',
  `celular` VARCHAR(45) NULL COMMENT '',
  `idtiporesidencia` INT NULL COMMENT '',
  `conviviente` VARCHAR(200) NULL COMMENT '',
  `sosten_economico` VARCHAR(100) NULL COMMENT '',
  `sosten_familia` VARCHAR(100) NULL COMMENT '',
  `edad_hijos` VARCHAR(45) NULL COMMENT '',
  `idtrabajo` INT NULL COMMENT '',
  `idtitulo` INT NULL COMMENT '',
  `iddeporte` INT NULL COMMENT '',
  `idhorastrabajo` INT NULL COMMENT '',
  PRIMARY KEY (`idalumno`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_alumno_persona1_idx` ON `tutoriasdb`.`alumno` (`idpersona` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_deporte1_idx` ON `tutoriasdb`.`alumno` (`iddeporte` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_nacionalidad1_idx` ON `tutoriasdb`.`alumno` (`idnacionalidad` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_ciudad1_idx` ON `tutoriasdb`.`alumno` (`idciudad_origen` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_titulo1_idx` ON `tutoriasdb`.`alumno` (`idtitulo` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_provincia1_idx` ON `tutoriasdb`.`alumno` (`idprovincia` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_trabajo1_idx` ON `tutoriasdb`.`alumno` (`idtrabajo` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_residencia1_idx` ON `tutoriasdb`.`alumno` (`idtiporesidencia` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_horasTrabajo1_idx` ON `tutoriasdb`.`alumno` (`idhorastrabajo` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`observacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`observacion` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`observacion` (
  `idobservacion` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `contenido` TEXT(4096) NULL COMMENT '',
  PRIMARY KEY (`idobservacion`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`reunion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`reunion` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`reunion` (
  `idreunion` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idalumno` INT NOT NULL COMMENT '',
  `idtutor` INT NOT NULL COMMENT '',
  `idobservacion` INT NOT NULL COMMENT '',
  `fecha` DATETIME NULL COMMENT '',
  `estado` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idreunion`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_reunion_alumno1_idx` ON `tutoriasdb`.`reunion` (`idalumno` ASC)  COMMENT '';

CREATE INDEX `fk_reunion_tutor1_idx` ON `tutoriasdb`.`reunion` (`idtutor` ASC)  COMMENT '';

CREATE INDEX `fk_reunion_observacion1_idx` ON `tutoriasdb`.`reunion` (`idobservacion` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`tipoObservacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`tipoObservacion` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`tipoObservacion` (
  `idtipoObservacion` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idtipoObservacion`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`mensaje`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`mensaje` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`mensaje` (
  `idmensaje` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idautor` INT NOT NULL COMMENT '',
  `idreceptor` INT NOT NULL COMMENT '',
  `contenido` VARCHAR(254) NULL COMMENT '',
  `tipo` INT NULL COMMENT 'diferencia entre mensaje y sugerencia de cartelera',
  PRIMARY KEY (`idmensaje`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_mensaje_persona1_idx` ON `tutoriasdb`.`mensaje` (`idautor` ASC)  COMMENT '';

CREATE INDEX `fk_mensaje_persona2_idx` ON `tutoriasdb`.`mensaje` (`idreceptor` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`tipoObservacion_observacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`tipoObservacion_observacion` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`tipoObservacion_observacion` (
  `idtipoObservacionobservacion` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idtipoObservacion` INT NOT NULL COMMENT '',
  `idobservacion` INT NOT NULL COMMENT '',
  PRIMARY KEY (`idtipoObservacionobservacion`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_tipoObservacion_has_observacion_observacion1_idx` ON `tutoriasdb`.`tipoObservacion_observacion` (`idobservacion` ASC)  COMMENT '';

CREATE INDEX `fk_tipoObservacion_has_observacion_tipoObservacion1_idx` ON `tutoriasdb`.`tipoObservacion_observacion` (`idtipoObservacion` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`disminucion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`disminucion` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`disminucion` (
  `iddisminucion` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`iddisminucion`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`alumno_disminucion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`alumno_disminucion` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`alumno_disminucion` (
  `id_alumno_disminucion` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idalumno` INT NULL COMMENT '',
  `iddisminucion` INT NULL COMMENT '',
  PRIMARY KEY (`id_alumno_disminucion`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_alumno_has_disminucion_disminucion1_idx` ON `tutoriasdb`.`alumno_disminucion` (`iddisminucion` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_has_disminucion_alumno1_idx` ON `tutoriasdb`.`alumno_disminucion` (`idalumno` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`ayuda`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`ayuda` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`ayuda` (
  `idayuda` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  `idalumno` INT NULL COMMENT '',
  PRIMARY KEY (`idayuda`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_ayuda_alumno1_idx` ON `tutoriasdb`.`ayuda` (`idalumno` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`colegio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`colegio` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`colegio` (
  `idcolegio` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idcolegio`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`cursocolegio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`cursocolegio` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`cursocolegio` (
  `idcurso` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idcolegio` INT NULL COMMENT '',
  `idalumno` INT NULL COMMENT '',
  `idciudad` INT NULL COMMENT '',
  `fechagraduacion` DATE NULL COMMENT '',
  PRIMARY KEY (`idcurso`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_cursocolegio_ciudad1_idx` ON `tutoriasdb`.`cursocolegio` (`idciudad` ASC)  COMMENT '';

CREATE INDEX `fk_cursocolegio_alumno1_idx` ON `tutoriasdb`.`cursocolegio` (`idalumno` ASC)  COMMENT '';

CREATE INDEX `fk_cursocolegio_colegio1_idx` ON `tutoriasdb`.`cursocolegio` (`idcolegio` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`franjaHoraria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`franjaHoraria` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`franjaHoraria` (
  `idfranjaHoraria` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idfranjaHoraria`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`materia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`materia` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`materia` (
  `idmateria` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idmateria`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`alumno_materia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`alumno_materia` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`alumno_materia` (
  `idalumno_materia` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idmateria` INT NULL COMMENT '',
  `idalumno` INT NULL COMMENT '',
  `horaentrada` TIME NULL COMMENT '',
  `horasalida` TIME NULL COMMENT '',
  `dia` INT NULL COMMENT '',
  PRIMARY KEY (`idalumno_materia`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_alumno_materia_materia1_idx` ON `tutoriasdb`.`alumno_materia` (`idmateria` ASC)  COMMENT '';

CREATE INDEX `fk_alumno_materia_alumno1_idx` ON `tutoriasdb`.`alumno_materia` (`idalumno` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`funcion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`funcion` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`funcion` (
  `idfuncion` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `descripcion` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`idfuncion`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tutoriasdb`.`perfil_funcion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`perfil_funcion` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`perfil_funcion` (
  `idperfil_funcion` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `idperfil` INT NOT NULL COMMENT '',
  `idfuncion` INT NOT NULL COMMENT '',
  PRIMARY KEY (`idperfil_funcion`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_perfil_funcion_perfil1_idx` ON `tutoriasdb`.`perfil_funcion` (`idperfil` ASC)  COMMENT '';

CREATE INDEX `fk_perfil_funcion_funcion1_idx` ON `tutoriasdb`.`perfil_funcion` (`idfuncion` ASC)  COMMENT '';


-- -----------------------------------------------------
-- Table `tutoriasdb`.`franjaHoraria_alumno`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tutoriasdb`.`franjaHoraria_alumno` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`franjaHoraria_alumno` (
  `idfranjaHoraria_alumno` INT NOT NULL COMMENT '',
  `idfranjaHoraria` INT NULL COMMENT '',
  `idalumno` INT NULL COMMENT '',
  PRIMARY KEY (`idfranjaHoraria_alumno`)  COMMENT '')
ENGINE = InnoDB;

CREATE INDEX `fk_franjaHoraria_has_alumno_alumno1_idx` ON `tutoriasdb`.`franjaHoraria_alumno` (`idalumno` ASC)  COMMENT '';

CREATE INDEX `fk_franjaHoraria_has_alumno_franjaHoraria1_idx` ON `tutoriasdb`.`franjaHoraria_alumno` (`idfranjaHoraria` ASC)  COMMENT '';

USE `tutoriasdb` ;

CREATE TABLE IF NOT EXISTS `tutoriasdb`.`permisos` (`idpersona` INT, `nombre` INT, `apellido` INT, `legajo` INT, `email` INT, `hash` INT, `sal` INT, `idperfil` INT, `activo` INT, `descripcion` INT, `idperfil_funcion` INT, `idfuncion` INT);



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
