<?php

namespace tutoriasBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use tutoriasBundle\Entity\Nacionalidad;
use tutoriasBundle\Entity\Tiporesidencia;
use tutoriasBundle\Entity\Alumno;
use tutoriasBundle\Entity\Persona;
use tutoriasBundle\Entity\Ayuda;
use tutoriasBundle\Entity\Colegio;
use tutoriasBundle\Entity\Mensaje;
use tutoriasBundle\Entity\Reunion;
use tutoriasBundle\Entity\Tutor;
use tutoriasBundle\Entity\Asignado;
use tutoriasBundle\Entity\Observacion;
use tutoriasBundle\Entity\TipoObservacion;
use tutoriasBundle\Entity\Cursa;
use tutoriasBundle\Entity\Materia;
use tutoriasBundle\Entity\Trabajo;
use tutoriasBundle\Entity\Franjahoraria;
use tutoriasBundle\Entity\Horastrabajo;
use tutoriasBundle\Entity\Disminucion;
use tutoriasBundle\Entity\Titulo;
use tutoriasBundle\Entity\Ciudad;
use tutoriasBundle\Entity\Deporte;
use tutoriasBundle\Entity\Perfil;
use tutoriasBundle\Entity\Funcion;
use phpmailer\phpmailer;








class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('tutoriasBundle:Default:index.html.twig', array('name' => $name));
    }

	public function loginAction(){
    	return $this->render('tutoriasBundle:Default:login.html.twig');	
    }

    public function validacionAction(){
    	

			

    	$request = $this->getRequest(); 
	    $u=$request->request->get('usuario');
	    $p=$request->request->get('password');
	    // NO ESTA PASANDO EL IDPERFIL
	    // CON ES IDPEFIL MANERAJIRA EL PERFIL
	    // LAS VISTAS PARA CADA UNA DE LOS USUARIOS

	    if($this->loginCheck($u,$p)){
			return $this->render('tutoriasBundle:Default:validacion.html.twig',array('usuario'=>$u,'estado'=>'ok','perfil'=>$_SESSION['perfil'],'tipo'=>$_SESSION['tipo']));	
    	}
    	else{
    		return $this->render('tutoriasBundle:Default:validacion.html.twig',array('usuario'=>$u,'estado'=>'error'));
    	}
    }

    public function verAction(){
		return $this->render('tutoriasBundle:Default:mailer.html.twig');
	}

	public function mailerAction() 
	{ 
		if ($this->VerificoPerfil()){ 
	  		return $this->render('tutoriasBundle:Default:mailer.html.twig'); 
		} 
		else{ 
			return $this->loginAction(); 
		} 
	}

	public function envioAction(){

    	$request = $this->getRequest(); 
	    $u=$request->request->get('cuerpo');
	    $p=$request->request->get('asunto');
	    print_r($u);
	    print_r($p);
	    if($this->loginCheck($u,$p)){
			return $this->render('tutoriasBundle:Default:validacion.html.twig',array('usuario'=>$u,'estado'=>'ok'));	
    	}
    	else{
    		return $this->render('tutoriasBundle:Default:validacion.html.twig',array('usuario'=>$u,'estado'=>'error'));
    	}
    }


	public function mailer_enviarAction(){
		/*
		require 'PHPMailer\PHPMailerAutoload.php';

		$mail = new PHPMailer;

		*/
    	$request = $this->getRequest(); 
	    $cuerpo=$request->request->get('cuerpo');
	    $asunto=$request->request->get('asunto');
	    print_r($cuerpo);
	    print_r($asunto);
 
     /*
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
		$mail->Port = 465;  
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'tutoriasmedicinaunlp@gmail.com';                 // SMTP username
		$mail->Password = 'jFV4lBuP2[CJu5l';                           // SMTP password
		$mail->SMTPSecure = 'ssl';                            // Enable encryption, 'ssl' also accepted

		//$mail->SMTPDebug = 3;
		$mail->From = 'tutoriasmedicinaunlp@gmail.com';
		$mail->FromName = 'Activacion de Cuenta Tutorias Facultad de Medicina';
		$mail->addAddress('jarpa07@gmail.com', 'Lucas Jarpa ');
		

		$mail->WordWrap = 50;                               // Optional name
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $asunto;
		$mail->Body    = $cuerpo;
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		  	return $this->render('tutoriasBundle:Default:mailer.html.twig');  
		}
		*/
		return $this->render('tutoriasBundle:Default:mailer.html.twig');
		
	}

	    

    //**//  Funcionalidades ABMs   //**//


	public function alta_nacionalidadAction() 
	{ 
		if ($this->VerificoPerfil()){ 
	  		return $this->render('tutoriasBundle:Default:alta_nacionalidad.html.twig'); 
		} 
		else{ 
			return $this->loginAction(); 
		} 
	}

	public function alta_nacionalidad_inAction() 
	{ 
	   if ($this->VerificoPerfil()){ 
	       $request = $this->getRequest(); 
	       $descripcion=$request->request->get('descripcion'); 
	       $nacionalidad= new Nacionalidad(); 
	       $nacionalidad->setDescripcion($descripcion); 
	       $em = $this->getDoctrine()->getManager(); 
	       $em->persist($nacionalidad); 
	       $em->flush(); 
	       $result='Operacion exitosa!'; 
	       return $this->render('tutoriasBundle:Default:alta_nacionalidad.html.twig');
	   }
	   else{
	       return $this->loginAction();
	   }

	}

	public function alta_alumnoAction() 
	{ 
	  if ($this->VerificoPerfil()){
	  	$nacionalidad= $this->getDoctrine()->getRepository('tutoriasBundle:Nacionalidad')->findAll();
	  	$deporte = $this->getDoctrine()->getRepository('tutoriasBundle:Deporte')->findAll();
	  	$ciudad= $this->getDoctrine()->getRepository('tutoriasBundle:Ciudad')->findAll();
	  	$ayuda= $this->getDoctrine()->getRepository('tutoriasBundle:Ayuda')->findAll();
	  	$colegio= $this->getDoctrine()->getRepository('tutoriasBundle:Colegio')->findAll();
	  	$trabajo= $this->getDoctrine()->getRepository('tutoriasBundle:Trabajo')->findAll();
	  	$horastrabajo= $this->getDoctrine()->getRepository('tutoriasBundle:Horastrabajo')->findAll();
		$franja= $this->getDoctrine()->getRepository('tutoriasBundle:Franjahoraria')->findAll();	

	    return $this->render('tutoriasBundle:Default:alta_alumno.html.twig',array('nacionalidad'=>$nacionalidad,'deporte'=>$deporte,'ciudad')); 
	  } 
   	  else{ 
   		 return $this->loginAction();	 
   	  } 
 	}

 	public function alta_alumno_inAction() 
	{ 
	   if ($this->VerificoPerfil()){ 
	       $request = $this->getRequest(); 
	       $idnacionalidad=$request->request->get('idnacionalidad'); 
	       $idPersona=$request->request->get('idPersona'); 
	       $iddeporte=$request->request->get('idDeporte'); 
	       $idciudad=$request->request->get('idCiudad'); 
	       $idayuda=$request->request->get('idAyuda'); 
	       $idcolegio=$request->request->get('idColegio'); 
	       $idtrabajo=$request->request->get('idTrabajo'); 
	       $idhorastrabajo=$request->request->get('idHorastrabajo'); 
	       $idfranjahoraria=$request->request->get('idFranjahoraria'); 
	       $idtitulo=$request->request->get('idTitulo'); 
	       $alumno= new Alumno(); 
	       $nacionalidad= $this->getDoctrine()->getRepository('tutoriasBundle:Nacionalidad')->findByid($idnacionalidad); 
	       $alumno=setnacionalidad($nacionalidad); 
	       $Persona= $this->getDoctrine()->getRepository('tutoriasBundle:Persona')->findByid($idPersona); 
	       $alumno=setPersona($Persona); 
	       $deporte= $this->getDoctrine()->getRepository('tutoriasBundle:Deporte')->findByid($iddeporte); 
	       $alumno=setdeporte($deporte); 
	       $ciudad= $this->getDoctrine()->getRepository('tutoriasBundle:Ciudad')->findByid($idciudad); 
	       $alumno=setciudad($ciudad); 
	       $ayuda= $this->getDoctrine()->getRepository('tutoriasBundle:Ayuda')->findByid($idayuda); 
	       $alumno=setayuda($ayuda); 
	       $colegio= $this->getDoctrine()->getRepository('tutoriasBundle:Colegio')->findByid($idcolegio); 
	       $alumno=setcolegio($colegio); 
	       $trabajo= $this->getDoctrine()->getRepository('tutoriasBundle:Trabajo')->findByid($idtrabajo); 
	       $alumno=settrabajo($trabajo); 
	       $horastrabajo= $this->getDoctrine()->getRepository('tutoriasBundle:Horastrabajo')->findByid($idhorastrabajo); 
	       $alumno=sethorastrabajo($horastrabajo); 
	       $franjahoraria= $this->getDoctrine()->getRepository('tutoriasBundle:Franjahoraria')->findByid($idfranjahoraria); 
	       $alumno=setfranjahoraria($franjahoraria); 
	       $titulo= $this->getDoctrine()->getRepository('tutoriasBundle:Titulo')->findByid($idtitulo); 
	       $alumno=settitulo($titulo); 
	       $em = $this->getDoctrine()->getManager(); 
	       $em->persist($alumno); 
	       $em->flush(); 
	       
	       return $this->render('tutoriasBundle:Default:alta_alumno.html.twig');
	   }
	   else{
	       return $this->loginAction();
	   }

	}


 	
 	public function alta_personaAction() 
	{ 
	  	if ($this->VerificoPerfil()){

	  		$perfiles = $this->getDoctrine()->getRepository('tutoriasBundle:Perfil')->findAll();

	      	return $this->render('tutoriasBundle:Default:alta_persona.html.twig',array('perfil'=>$perfiles)); 
	  	} 
	   	else{ 
	   		return $this->loginAction(); 
	   	}	 
 	} 

	public function alta_persona_inAction() 
	{ 
   	if ($this->VerificoPerfil()){ 
       $request = $this->getRequest(); 
        
       $nombre=$request->request->get('nombre'); 
       $apellido=$request->request->get('apellido'); 
       $legajo=$request->request->get('legajo'); 
       $email=$request->request->get('email'); 
       $password=$request->request->get('password'); 
       $salCodifided = openssl_random_pseudo_bytes(32);
       //$sal = (string)$sal;
       //$sal	=rand(5, 15);
       $sal = utf8_encode($salCodifided);
       $hash = openssl_digest($password.$sal,'sha512');
       $dni=$request->request->get('dni'); 
       $idperfil=$request->request->get('Perfil'); 
       $persona= new Persona();
       $persona->setIdperfil($idperfil); 
       $persona->setNombre($nombre); 
       $persona->setApellido($apellido); 
       $persona->setLegajo($legajo); 
       $persona->setEmail($email); 
       $persona->setHash($hash); 
       $persona->setSal($sal);
       $persona->setActivo(0);
       $em = $this->getDoctrine()->getManager(); 
       $em->persist($persona); 
       $em->flush(); 
        
       return $this->render('tutoriasBundle:Default:alta_persona.html.twig');
   }
   else{
       return $this->loginAction();
   }

}



	public function alta_perfilAction() 
	{ 
  		if ($this->VerificoPerfil()){ 
      		return $this->render('tutoriasBundle:Default:alta_perfil.html.twig'); 
  	} 
   		else{ 
   			return $this->loginAction(); 
   	} 
 	}

 	public function alta_perfil_inAction() 
	{ 
		if ($this->VerificoPerfil()){ 
		   $request = $this->getRequest(); 
		   $descripcion=$request->request->get('descripcion'); 
		   $perfil= new Perfil(); 
		   $perfil->setDescripcion($descripcion); 
		   $em = $this->getDoctrine()->getManager(); 
		   $em->persist($perfil); 
		   $em->flush(); 
		    
		   return $this->render('tutoriasBundle:Default:alta_perfil.html.twig');
		}
		else{
		   return $this->loginAction();
		}

	} 





 	

	public function alta_deporteAction() 
	{ 
	  if ($this->VerificoPerfil()){ 
	      return $this->render('tutoriasBundle:Default:alta_deporte.html.twig'); 
	  } 
	  else{ 
	   	  return $this->loginAction(); 
	  } 
	} 
	 public function alta_deporte_inAction() 
	{ 
	   if ($this->VerificoPerfil()){ 
	       $request = $this->getRequest(); 
	       $descripcion=$request->request->get('descripcion'); 
	       $deporte= new Deporte(); 
	       $deporte->setDescripcion($descripcion); 
	       $em = $this->getDoctrine()->getManager(); 
	       $em->persist($deporte); 
	       $em->flush(); 
	        
	       return $this->render('tutoriasBundle:Default:alta_deporte.html.twig');
	   }
	   else{
	       return $this->loginAction();
	   }

	}


	public function alta_funcionAction() 
	{ 
	  if ($this->VerificoPerfil()){ 


	      return $this->render('tutoriasBundle:Default:alta_funcion.html.twig'); 
	  } 
	   else{ 
	   	  return $this->loginAction(); 
	   } 
	} 
	 public function alta_funcion_inAction() 
	{ 
	   if ($this->VerificoPerfil()){ 
	       $request = $this->getRequest(); 
	       $descripcion=$request->request->get('descripcion'); 
	       $funcion= new funcion(); 
	       $funcion->setDescripcion($descripcion); 
	       $em = $this->getDoctrine()->getManager(); 
	       $em->persist($funcion); 
	       $em->flush(); 
	        
	       return $this->render('tutoriasBundle:Default:alta_funcion.html.twig');
	   }
	   else{
	       return $this->loginAction();
	   }

	}

	public function alta_ciudadAction() 
	{ 
	  if ($this->VerificoPerfil()){ 
	      return $this->render('tutoriasBundle:Default:alta_ciudad.html.twig'); 
	  } 
	   else{ 
	   	  return $this->loginAction(); 
	   } 
	}

	 public function alta_ciudad_inAction() 
	{ 
	   if ($this->VerificoPerfil()){ 
	       $request = $this->getRequest(); 
	       $nombre=$request->request->get('nombre'); 
	       $ciudad= new Ciudad(); 
	       $ciudad->setNombre($nombre); 
	       $em = $this->getDoctrine()->getManager(); 
	       $em->persist($ciudad); 
	       $em->flush(); 
	       
	       return $this->render('tutoriasBundle:Default:alta_ciudad.html.twig');
	   }
	   else{
	       return $this->loginAction();
	   }

	}

	public function alta_tutorAction() 
	{ 
	  if ($this->VerificoPerfil()){ 
	     return $this->render('tutoriasBundle:Default:alta_tutor.html.twig'); 
	  } 
	   else{ 
	   		return $this->loginAction(); 
	   } 
	}

	 public function alta_tutor_inAction() 
	{ 
	   if ($this->VerificoPerfil()){ 
	       $request = $this->getRequest(); 
	       $idPersona=$request->request->get('idPersona'); 
	       $tutor= new Tutor(); 
	       $Persona= $this->getDoctrine()->getRepository('tutoriasBundle:Persona')->findByid($idPersona); 
	       $tutor=setPersona($Persona); 
	       $em = $this->getDoctrine()->getManager(); 
	       $em->persist($tutor); 
	       $em->flush(); 
	        
	       return $this->render('TutoriasBundle:Default:alta_tutor.html.twig');
	   }
	   else{
	       return $this->loginAction();
	   }

	}

	public function show_tutorAction()
	   {
	       if ($this->VerificoPerfil()){  
	           $repository= $this->getDoctrine()->getRepository('tutoriasBundle:tutor');
	           $tutor=$repository->findAll();            
	           return $this->render('tutoriasBundle:Default:list_tutor.html.twig', array('tutor'=> $tutors));
	       }
	       else{
	           return $this->loginAction();
	       }         
	   }    

	public function mod_tutorAction() 
	{ 
	    if ($this->VerificoPerfil('mod_tutor')){ 
	       $request = $this->getRequest(); 
	       $idtutor = $request->request->get('idtutor'); 
	       $em = $this->getDoctrine()->getManager(); 
	       $tutor = $this->getDoctrine()->getRepository('tutoriasBundle:tutor')->findByid($idtutor); 
	        return $this->render('tutoriasBundle:Default:mod_tutor.html.twig', array('usuario' => $_SESSION['Usuario'], 'tutor'=> $tutor)); 
	    } 
	    else{ 
	        $this->loginAction(); 
	    } 
	} 

	public function mod_tutor_inAction() 
	  { 
	      if ($this->VerificoPerfil('mod_tutor_in')){ 
	          $request = $this->getRequest();  
	          $idtutor = $request->request->get('Idtutor'); 
	          $em = $this->getDoctrine()->getManager(); 
	          $tutor = $this->getDoctrine()->getRepository('tutoriasBundle:tutor')->findById($idtutor); 
	          $idPersona = $request->request->get('idPersona'); 
	          $tutor = $this->getDoctrine()->getRepository('tutoriasBundle:tutor')->findById($idtutor); 
	          $tutor->setidtutor($tutor); 
	          $Persona = $this->getDoctrine()->getRepository('tutoriasBundle:Persona')->findById($idPersona); 
	          $tutor->setidPersona($Persona); 
	          $em->flush(); 
	          $result=('Operacion exitosa!'); 
	          return $this->render('tutoriasBundle:Default:mod_tutor.html.twig',array('usuario' => $_SESSION['Usuario'], 'result'=>$result, 'tutor'=>$tutor)); 
	      } 
	      else{ 
	          $this->loginAction(); 
	      } 
	  } 




	//**//

    //**//  Funcionalidades de seguridad interna , login, hash //**//

	public function VerificoPerfil(){
		return true;
	} 

	public function loginCheck($u,$p)
  	{
	    
	    $Miusuario = $this->getDoctrine()->getRepository('tutoriasBundle:Persona')->findOneByLegajo($u);
	    $Miperfil = $Miusuario->getIdperfil();
	    $unPerfil = $this->getDoctrine()->getRepository('tutoriasBundle:Perfil')->findOneByIdperfil($Miperfil);

	    
	    $sal = $Miusuario->getSal();
	    
	    $hash = openssl_digest($p.$sal,'sha512');
	    
	    $checkeado=false;
	    if($hash == $Miusuario->getHash() )
	    {
	    	//print_r($p);
	    	//print_r($sal);
	    	//print_r($hash);
	    	//$distro = $Miusuario->getHash();
	    	//echo "<br>";
	    	//print_r($distro	);

	        $_SESSION['logueado']='true';
	        $_SESSION['idPersona']=$Miusuario->getIdpersona();
	       	$_SESSION['perfil']= $Miusuario->getIdperfil();
	       	$_SESSION['tipo'] = $unPerfil->getDescripcion();
	        $_SESSION['saludo'] = 'Ingreso';
	        

	        return true;
	    }
	    else
	    {
	    	$_SESSION['saludo'] = 'error de login';
	    	return false;	
	    }
	    
  }


	//**//

}
