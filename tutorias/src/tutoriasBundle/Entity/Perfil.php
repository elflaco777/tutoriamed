<?php

namespace tutoriasBundle\Entity;

/**
 * Perfil
 */
class Perfil
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idperfil;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Perfil
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idperfil
     *
     * @return integer
     */
    public function getIdperfil()
    {
        return $this->idperfil;
    }
}

