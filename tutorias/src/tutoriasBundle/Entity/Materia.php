<?php

namespace tutoriasBundle\Entity;

/**
 * Materia
 */
class Materia
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idmateria;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Materia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idmateria
     *
     * @return integer
     */
    public function getIdmateria()
    {
        return $this->idmateria;
    }
}

