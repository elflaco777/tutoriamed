<?php

namespace tutoriasBundle\Entity;

/**
 * Nacionalidad
 */
class Nacionalidad
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idnacionalidad;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Nacionalidad
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idnacionalidad
     *
     * @return integer
     */
    public function getIdnacionalidad()
    {
        return $this->idnacionalidad;
    }
}

