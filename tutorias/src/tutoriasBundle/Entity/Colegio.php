<?php

namespace tutoriasBundle\Entity;

/**
 * Colegio
 */
class Colegio
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $idcolegio;


    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Colegio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get idcolegio
     *
     * @return integer
     */
    public function getIdcolegio()
    {
        return $this->idcolegio;
    }
}

