<?php

namespace tutoriasBundle\Entity;

/**
 * Cursocolegio
 */
class Cursocolegio
{
    /**
     * @var integer
     */
    private $idcolegio;

    /**
     * @var integer
     */
    private $idalumno;

    /**
     * @var integer
     */
    private $idciudad;

    /**
     * @var \DateTime
     */
    private $fechagraduacion;

    /**
     * @var integer
     */
    private $idcurso;


    /**
     * Set idcolegio
     *
     * @param integer $idcolegio
     *
     * @return Cursocolegio
     */
    public function setIdcolegio($idcolegio)
    {
        $this->idcolegio = $idcolegio;

        return $this;
    }

    /**
     * Get idcolegio
     *
     * @return integer
     */
    public function getIdcolegio()
    {
        return $this->idcolegio;
    }

    /**
     * Set idalumno
     *
     * @param integer $idalumno
     *
     * @return Cursocolegio
     */
    public function setIdalumno($idalumno)
    {
        $this->idalumno = $idalumno;

        return $this;
    }

    /**
     * Get idalumno
     *
     * @return integer
     */
    public function getIdalumno()
    {
        return $this->idalumno;
    }

    /**
     * Set idciudad
     *
     * @param integer $idciudad
     *
     * @return Cursocolegio
     */
    public function setIdciudad($idciudad)
    {
        $this->idciudad = $idciudad;

        return $this;
    }

    /**
     * Get idciudad
     *
     * @return integer
     */
    public function getIdciudad()
    {
        return $this->idciudad;
    }

    /**
     * Set fechagraduacion
     *
     * @param \DateTime $fechagraduacion
     *
     * @return Cursocolegio
     */
    public function setFechagraduacion($fechagraduacion)
    {
        $this->fechagraduacion = $fechagraduacion;

        return $this;
    }

    /**
     * Get fechagraduacion
     *
     * @return \DateTime
     */
    public function getFechagraduacion()
    {
        return $this->fechagraduacion;
    }

    /**
     * Get idcurso
     *
     * @return integer
     */
    public function getIdcurso()
    {
        return $this->idcurso;
    }
}

