<?php

namespace tutoriasBundle\Entity;

/**
 * Reunion
 */
class Reunion
{
    /**
     * @var integer
     */
    private $idalumno;

    /**
     * @var integer
     */
    private $idtutor;

    /**
     * @var integer
     */
    private $idobservacion;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var integer
     */
    private $idreunion;


    /**
     * Set idalumno
     *
     * @param integer $idalumno
     *
     * @return Reunion
     */
    public function setIdalumno($idalumno)
    {
        $this->idalumno = $idalumno;

        return $this;
    }

    /**
     * Get idalumno
     *
     * @return integer
     */
    public function getIdalumno()
    {
        return $this->idalumno;
    }

    /**
     * Set idtutor
     *
     * @param integer $idtutor
     *
     * @return Reunion
     */
    public function setIdtutor($idtutor)
    {
        $this->idtutor = $idtutor;

        return $this;
    }

    /**
     * Get idtutor
     *
     * @return integer
     */
    public function getIdtutor()
    {
        return $this->idtutor;
    }

    /**
     * Set idobservacion
     *
     * @param integer $idobservacion
     *
     * @return Reunion
     */
    public function setIdobservacion($idobservacion)
    {
        $this->idobservacion = $idobservacion;

        return $this;
    }

    /**
     * Get idobservacion
     *
     * @return integer
     */
    public function getIdobservacion()
    {
        return $this->idobservacion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Reunion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Reunion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Get idreunion
     *
     * @return integer
     */
    public function getIdreunion()
    {
        return $this->idreunion;
    }
}

