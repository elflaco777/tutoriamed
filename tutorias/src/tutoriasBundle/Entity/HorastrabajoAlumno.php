<?php

namespace tutoriasBundle\Entity;

/**
 * HorastrabajoAlumno
 */
class HorastrabajoAlumno
{
    /**
     * @var integer
     */
    private $idhorasTrabajo;

    /**
     * @var integer
     */
    private $idalumno;

    /**
     * @var integer
     */
    private $idhorastrabajoAlumno;


    /**
     * Set idhorasTrabajo
     *
     * @param integer $idhorasTrabajo
     *
     * @return HorastrabajoAlumno
     */
    public function setIdhorasTrabajo($idhorasTrabajo)
    {
        $this->idhorasTrabajo = $idhorasTrabajo;

        return $this;
    }

    /**
     * Get idhorasTrabajo
     *
     * @return integer
     */
    public function getIdhorasTrabajo()
    {
        return $this->idhorasTrabajo;
    }

    /**
     * Set idalumno
     *
     * @param integer $idalumno
     *
     * @return HorastrabajoAlumno
     */
    public function setIdalumno($idalumno)
    {
        $this->idalumno = $idalumno;

        return $this;
    }

    /**
     * Get idalumno
     *
     * @return integer
     */
    public function getIdalumno()
    {
        return $this->idalumno;
    }

    /**
     * Get idhorastrabajoAlumno
     *
     * @return integer
     */
    public function getIdhorastrabajoAlumno()
    {
        return $this->idhorastrabajoAlumno;
    }
}

