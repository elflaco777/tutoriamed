<?php

namespace tutoriasBundle\Entity;

/**
 * Tiporesidencia
 */
class Tiporesidencia
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idtiporesidencia;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Tiporesidencia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idtiporesidencia
     *
     * @return integer
     */
    public function getIdtiporesidencia()
    {
        return $this->idtiporesidencia;
    }
}

