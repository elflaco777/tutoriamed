<?php

namespace tutoriasBundle\Entity;

/**
 * AlumnoDisminucion
 */
class AlumnoDisminucion
{
    /**
     * @var integer
     */
    private $idalumno;

    /**
     * @var integer
     */
    private $iddisminucion;

    /**
     * @var integer
     */
    private $idAlumnoDisminucion;


    /**
     * Set idalumno
     *
     * @param integer $idalumno
     *
     * @return AlumnoDisminucion
     */
    public function setIdalumno($idalumno)
    {
        $this->idalumno = $idalumno;

        return $this;
    }

    /**
     * Get idalumno
     *
     * @return integer
     */
    public function getIdalumno()
    {
        return $this->idalumno;
    }

    /**
     * Set iddisminucion
     *
     * @param integer $iddisminucion
     *
     * @return AlumnoDisminucion
     */
    public function setIddisminucion($iddisminucion)
    {
        $this->iddisminucion = $iddisminucion;

        return $this;
    }

    /**
     * Get iddisminucion
     *
     * @return integer
     */
    public function getIddisminucion()
    {
        return $this->iddisminucion;
    }

    /**
     * Get idAlumnoDisminucion
     *
     * @return integer
     */
    public function getIdAlumnoDisminucion()
    {
        return $this->idAlumnoDisminucion;
    }
}

