<?php

namespace tutoriasBundle\Entity;

/**
 * TipoobservacionObservacion
 */
class TipoobservacionObservacion
{
    /**
     * @var integer
     */
    private $idtipoobservacion;

    /**
     * @var integer
     */
    private $idobservacion;

    /**
     * @var integer
     */
    private $idtipoobservacionobservacion;


    /**
     * Set idtipoobservacion
     *
     * @param integer $idtipoobservacion
     *
     * @return TipoobservacionObservacion
     */
    public function setIdtipoobservacion($idtipoobservacion)
    {
        $this->idtipoobservacion = $idtipoobservacion;

        return $this;
    }

    /**
     * Get idtipoobservacion
     *
     * @return integer
     */
    public function getIdtipoobservacion()
    {
        return $this->idtipoobservacion;
    }

    /**
     * Set idobservacion
     *
     * @param integer $idobservacion
     *
     * @return TipoobservacionObservacion
     */
    public function setIdobservacion($idobservacion)
    {
        $this->idobservacion = $idobservacion;

        return $this;
    }

    /**
     * Get idobservacion
     *
     * @return integer
     */
    public function getIdobservacion()
    {
        return $this->idobservacion;
    }

    /**
     * Get idtipoobservacionobservacion
     *
     * @return integer
     */
    public function getIdtipoobservacionobservacion()
    {
        return $this->idtipoobservacionobservacion;
    }
}

