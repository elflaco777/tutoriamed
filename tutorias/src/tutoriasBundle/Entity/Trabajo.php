<?php

namespace tutoriasBundle\Entity;

/**
 * Trabajo
 */
class Trabajo
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idtrabajo;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Trabajo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idtrabajo
     *
     * @return integer
     */
    public function getIdtrabajo()
    {
        return $this->idtrabajo;
    }
}

