<?php

namespace tutoriasBundle\Entity;

/**
 * Tipoobservacion
 */
class Tipoobservacion
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idtipoobservacion;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Tipoobservacion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idtipoobservacion
     *
     * @return integer
     */
    public function getIdtipoobservacion()
    {
        return $this->idtipoobservacion;
    }
}

