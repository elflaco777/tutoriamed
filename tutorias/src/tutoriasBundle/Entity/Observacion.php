<?php

namespace tutoriasBundle\Entity;

/**
 * Observacion
 */
class Observacion
{
    /**
     * @var string
     */
    private $contenido;

    /**
     * @var integer
     */
    private $idobservacion;


    /**
     * Set contenido
     *
     * @param string $contenido
     *
     * @return Observacion
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Get idobservacion
     *
     * @return integer
     */
    public function getIdobservacion()
    {
        return $this->idobservacion;
    }
}

