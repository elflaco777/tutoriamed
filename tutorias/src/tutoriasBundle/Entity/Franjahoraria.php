<?php

namespace tutoriasBundle\Entity;

/**
 * Franjahoraria
 */
class Franjahoraria
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idfranjahoraria;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Franjahoraria
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idfranjahoraria
     *
     * @return integer
     */
    public function getIdfranjahoraria()
    {
        return $this->idfranjahoraria;
    }
}

