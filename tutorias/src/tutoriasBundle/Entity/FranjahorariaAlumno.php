<?php

namespace tutoriasBundle\Entity;

/**
 * FranjahorariaAlumno
 */
class FranjahorariaAlumno
{
    /**
     * @var integer
     */
    private $idfranjahoraria;

    /**
     * @var integer
     */
    private $idalumno;

    /**
     * @var integer
     */
    private $idfranjahorariaAlumno;


    /**
     * Set idfranjahoraria
     *
     * @param integer $idfranjahoraria
     *
     * @return FranjahorariaAlumno
     */
    public function setIdfranjahoraria($idfranjahoraria)
    {
        $this->idfranjahoraria = $idfranjahoraria;

        return $this;
    }

    /**
     * Get idfranjahoraria
     *
     * @return integer
     */
    public function getIdfranjahoraria()
    {
        return $this->idfranjahoraria;
    }

    /**
     * Set idalumno
     *
     * @param integer $idalumno
     *
     * @return FranjahorariaAlumno
     */
    public function setIdalumno($idalumno)
    {
        $this->idalumno = $idalumno;

        return $this;
    }

    /**
     * Get idalumno
     *
     * @return integer
     */
    public function getIdalumno()
    {
        return $this->idalumno;
    }

    /**
     * Get idfranjahorariaAlumno
     *
     * @return integer
     */
    public function getIdfranjahorariaAlumno()
    {
        return $this->idfranjahorariaAlumno;
    }
}

