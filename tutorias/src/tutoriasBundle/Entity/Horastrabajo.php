<?php

namespace tutoriasBundle\Entity;

/**
 * Horastrabajo
 */
class Horastrabajo
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idhorasTrabajo;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Horastrabajo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idhorasTrabajo
     *
     * @return integer
     */
    public function getIdhorasTrabajo()
    {
        return $this->idhorasTrabajo;
    }
}

