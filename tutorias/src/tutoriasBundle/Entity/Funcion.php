<?php

namespace tutoriasBundle\Entity;

/**
 * Funcion
 */
class Funcion
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idfuncion;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Funcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idfuncion
     *
     * @return integer
     */
    public function getIdfuncion()
    {
        return $this->idfuncion;
    }
}

