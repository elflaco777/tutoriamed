<?php

namespace tutoriasBundle\Entity;

/**
 * Mensaje
 */
class Mensaje
{
    /**
     * @var integer
     */
    private $idautor;

    /**
     * @var integer
     */
    private $idreceptor;

    /**
     * @var string
     */
    private $contenido;

    /**
     * @var integer
     */
    private $tipo;

    /**
     * @var integer
     */
    private $idmensaje;


    /**
     * Set idautor
     *
     * @param integer $idautor
     *
     * @return Mensaje
     */
    public function setIdautor($idautor)
    {
        $this->idautor = $idautor;

        return $this;
    }

    /**
     * Get idautor
     *
     * @return integer
     */
    public function getIdautor()
    {
        return $this->idautor;
    }

    /**
     * Set idreceptor
     *
     * @param integer $idreceptor
     *
     * @return Mensaje
     */
    public function setIdreceptor($idreceptor)
    {
        $this->idreceptor = $idreceptor;

        return $this;
    }

    /**
     * Get idreceptor
     *
     * @return integer
     */
    public function getIdreceptor()
    {
        return $this->idreceptor;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     *
     * @return Mensaje
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     *
     * @return Mensaje
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Get idmensaje
     *
     * @return integer
     */
    public function getIdmensaje()
    {
        return $this->idmensaje;
    }
}

