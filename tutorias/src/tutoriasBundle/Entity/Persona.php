<?php

namespace tutoriasBundle\Entity;

/**
 * Persona
 */
class Persona
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $apellido;

    /**
     * @var integer
     */
    private $legajo;

    /**
     * @var integer
     */
    private $dni;


    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $hash;

    /**
     * @var string
     */
    private $sal;

    /**
     * @var integer
     */
    private $idperfil;

    /**
     * @var boolean
     */
    private $activo;

    /**
     * @var integer
     */
    private $idpersona;


    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Persona
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return Persona
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set legajo
     *
     * @param integer $legajo
     *
     * @return Persona
     */
    public function setLegajo($legajo)
    {
        $this->legajo = $legajo;

        return $this;
    }

    /**
     * Get legajo
     *
     * @return integer
     */
    public function getLegajo()
    {
        return $this->legajo;
    }
    /**
     * Set dni
     *
     * @param integer $dni
     *
     * @return Persona
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get Dni
     *
     * @return integer
     */
    public function getDni()
    {
        return $this->dni;
    }


    /**
     * Set email
     *
     * @param string $email
     *
     * @return Persona
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return Persona
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set sal
     *
     * @param string $sal
     *
     * @return Persona
     */
    public function setSal($sal)
    {
        $this->sal = $sal;

        return $this;
    }

    /**
     * Get sal
     *
     * @return string
     */
    public function getSal()
    {
        return $this->sal;
    }

    /**
     * Set idperfil
     *
     * @param integer $idperfil
     *
     * @return Persona
     */
    public function setIdperfil($idperfil)
    {
        $this->idperfil = $idperfil;

        return $this;
    }

    /**
     * Get idperfil
     *
     * @return integer
     */
    public function getIdperfil()
    {
        return $this->idperfil;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Persona
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get idpersona
     *
     * @return integer
     */
    public function getIdpersona()
    {
        return $this->idpersona;
    }
}

