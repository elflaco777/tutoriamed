<?php

namespace tutoriasBundle\Entity;

/**
 * Titulo
 */
class Titulo
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idtitulo;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Titulo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idtitulo
     *
     * @return integer
     */
    public function getIdtitulo()
    {
        return $this->idtitulo;
    }
}

