<?php

namespace tutoriasBundle\Entity;

/**
 * Tutor
 */
class Tutor
{
    /**
     * @var integer
     */
    private $idpersona;

    /**
     * @var integer
     */
    private $idtutor;


    /**
     * Set idpersona
     *
     * @param integer $idpersona
     *
     * @return Tutor
     */
    public function setIdpersona($idpersona)
    {
        $this->idpersona = $idpersona;

        return $this;
    }

    /**
     * Get idpersona
     *
     * @return integer
     */
    public function getIdpersona()
    {
        return $this->idpersona;
    }

    /**
     * Get idtutor
     *
     * @return integer
     */
    public function getIdtutor()
    {
        return $this->idtutor;
    }
}

