<?php

namespace tutoriasBundle\Entity;

/**
 * Alumno
 */
class Alumno
{
    /**
     * @var integer
     */
    private $idPersona;

    /**
     * @var string
     */
    private $genero;

    /**
     * @var \DateTime
     */
    private $fechaNacimiento;

    /**
     * @var integer
     */
    private $idnacionalidad;

    /**
     * @var integer
     */
    private $idciudadOrigen;

    /**
     * @var integer
     */
    private $idprovincia;

    /**
     * @var integer
     */
    private $dni;

    /**
     * @var string
     */
    private $domicilioLocal;

    /**
     * @var string
     */
    private $numero;

    /**
     * @var integer
     */
    private $piso;

    /**
     * @var string
     */
    private $depto;

    /**
     * @var string
     */
    private $localidad;

    /**
     * @var string
     */
    private $telefono;

    /**
     * @var string
     */
    private $celular;

    /**
     * @var integer
     */
    private $idtiporesidencia;

    /**
     * @var string
     */
    private $conviviente;

    /**
     * @var string
     */
    private $sostenEconomico;

    /**
     * @var string
     */
    private $sostenFamilia;

    /**
     * @var string
     */
    private $edadHijos;

    /**
     * @var integer
     */
    private $idtrabajo;

    /**
     * @var integer
     */
    private $idtitulo;

    /**
     * @var integer
     */
    private $iddeporte;

    /**
     * @var integer
     */
    private $idalumno;


    /**
     * Set idPersona
     *
     * @param integer $idPersona
     *
     * @return Alumno
     */
    public function setIdPersona($idPersona)
    {
        $this->idPersona = $idPersona;

        return $this;
    }

    /**
     * Get idPersona
     *
     * @return integer
     */
    public function getIdPersona()
    {
        return $this->idPersona;
    }

    /**
     * Set genero
     *
     * @param string $genero
     *
     * @return Alumno
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     *
     * @return Alumno
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set idnacionalidad
     *
     * @param integer $idnacionalidad
     *
     * @return Alumno
     */
    public function setIdnacionalidad($idnacionalidad)
    {
        $this->idnacionalidad = $idnacionalidad;

        return $this;
    }

    /**
     * Get idnacionalidad
     *
     * @return integer
     */
    public function getIdnacionalidad()
    {
        return $this->idnacionalidad;
    }

    /**
     * Set idciudadOrigen
     *
     * @param integer $idciudadOrigen
     *
     * @return Alumno
     */
    public function setIdciudadOrigen($idciudadOrigen)
    {
        $this->idciudadOrigen = $idciudadOrigen;

        return $this;
    }

    /**
     * Get idciudadOrigen
     *
     * @return integer
     */
    public function getIdciudadOrigen()
    {
        return $this->idciudadOrigen;
    }

    /**
     * Set idprovincia
     *
     * @param integer $idprovincia
     *
     * @return Alumno
     */
    public function setIdprovincia($idprovincia)
    {
        $this->idprovincia = $idprovincia;

        return $this;
    }

    /**
     * Get idprovincia
     *
     * @return integer
     */
    public function getIdprovincia()
    {
        return $this->idprovincia;
    }

    /**
     * Set dni
     *
     * @param integer $dni
     *
     * @return Alumno
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return integer
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set domicilioLocal
     *
     * @param string $domicilioLocal
     *
     * @return Alumno
     */
    public function setDomicilioLocal($domicilioLocal)
    {
        $this->domicilioLocal = $domicilioLocal;

        return $this;
    }

    /**
     * Get domicilioLocal
     *
     * @return string
     */
    public function getDomicilioLocal()
    {
        return $this->domicilioLocal;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Alumno
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set piso
     *
     * @param integer $piso
     *
     * @return Alumno
     */
    public function setPiso($piso)
    {
        $this->piso = $piso;

        return $this;
    }

    /**
     * Get piso
     *
     * @return integer
     */
    public function getPiso()
    {
        return $this->piso;
    }

    /**
     * Set depto
     *
     * @param string $depto
     *
     * @return Alumno
     */
    public function setDepto($depto)
    {
        $this->depto = $depto;

        return $this;
    }

    /**
     * Get depto
     *
     * @return string
     */
    public function getDepto()
    {
        return $this->depto;
    }

    /**
     * Set localidad
     *
     * @param string $localidad
     *
     * @return Alumno
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return string
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Alumno
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     *
     * @return Alumno
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set idtiporesidencia
     *
     * @param integer $idtiporesidencia
     *
     * @return Alumno
     */
    public function setIdtiporesidencia($idtiporesidencia)
    {
        $this->idtiporesidencia = $idtiporesidencia;

        return $this;
    }

    /**
     * Get idtiporesidencia
     *
     * @return integer
     */
    public function getIdtiporesidencia()
    {
        return $this->idtiporesidencia;
    }

    /**
     * Set conviviente
     *
     * @param string $conviviente
     *
     * @return Alumno
     */
    public function setConviviente($conviviente)
    {
        $this->conviviente = $conviviente;

        return $this;
    }

    /**
     * Get conviviente
     *
     * @return string
     */
    public function getConviviente()
    {
        return $this->conviviente;
    }

    /**
     * Set sostenEconomico
     *
     * @param string $sostenEconomico
     *
     * @return Alumno
     */
    public function setSostenEconomico($sostenEconomico)
    {
        $this->sostenEconomico = $sostenEconomico;

        return $this;
    }

    /**
     * Get sostenEconomico
     *
     * @return string
     */
    public function getSostenEconomico()
    {
        return $this->sostenEconomico;
    }

    /**
     * Set sostenFamilia
     *
     * @param string $sostenFamilia
     *
     * @return Alumno
     */
    public function setSostenFamilia($sostenFamilia)
    {
        $this->sostenFamilia = $sostenFamilia;

        return $this;
    }

    /**
     * Get sostenFamilia
     *
     * @return string
     */
    public function getSostenFamilia()
    {
        return $this->sostenFamilia;
    }

    /**
     * Set edadHijos
     *
     * @param string $edadHijos
     *
     * @return Alumno
     */
    public function setEdadHijos($edadHijos)
    {
        $this->edadHijos = $edadHijos;

        return $this;
    }

    /**
     * Get edadHijos
     *
     * @return string
     */
    public function getEdadHijos()
    {
        return $this->edadHijos;
    }

    /**
     * Set idtrabajo
     *
     * @param integer $idtrabajo
     *
     * @return Alumno
     */
    public function setIdtrabajo($idtrabajo)
    {
        $this->idtrabajo = $idtrabajo;

        return $this;
    }

    /**
     * Get idtrabajo
     *
     * @return integer
     */
    public function getIdtrabajo()
    {
        return $this->idtrabajo;
    }

    /**
     * Set idtitulo
     *
     * @param integer $idtitulo
     *
     * @return Alumno
     */
    public function setIdtitulo($idtitulo)
    {
        $this->idtitulo = $idtitulo;

        return $this;
    }

    /**
     * Get idtitulo
     *
     * @return integer
     */
    public function getIdtitulo()
    {
        return $this->idtitulo;
    }

    /**
     * Set iddeporte
     *
     * @param integer $iddeporte
     *
     * @return Alumno
     */
    public function setIddeporte($iddeporte)
    {
        $this->iddeporte = $iddeporte;

        return $this;
    }

    /**
     * Get iddeporte
     *
     * @return integer
     */
    public function getIddeporte()
    {
        return $this->iddeporte;
    }

    /**
     * Get idalumno
     *
     * @return integer
     */
    public function getIdalumno()
    {
        return $this->idalumno;
    }
}

