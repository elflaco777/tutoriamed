<?php

namespace tutoriasBundle\Entity;

/**
 * Provincia
 */
class Provincia
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $idprovincia;


    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Provincia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get idprovincia
     *
     * @return integer
     */
    public function getIdprovincia()
    {
        return $this->idprovincia;
    }
}

