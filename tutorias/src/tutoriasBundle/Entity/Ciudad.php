<?php

namespace tutoriasBundle\Entity;

/**
 * Ciudad
 */
class Ciudad
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $idciudad;


    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Ciudad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get idciudad
     *
     * @return integer
     */
    public function getIdciudad()
    {
        return $this->idciudad;
    }
}

