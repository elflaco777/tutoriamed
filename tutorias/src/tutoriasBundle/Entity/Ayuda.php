<?php

namespace tutoriasBundle\Entity;

/**
 * Ayuda
 */
class Ayuda
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idalumno;

    /**
     * @var integer
     */
    private $idayuda;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Ayuda
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idalumno
     *
     * @param integer $idalumno
     *
     * @return Ayuda
     */
    public function setIdalumno($idalumno)
    {
        $this->idalumno = $idalumno;

        return $this;
    }

    /**
     * Get idalumno
     *
     * @return integer
     */
    public function getIdalumno()
    {
        return $this->idalumno;
    }

    /**
     * Get idayuda
     *
     * @return integer
     */
    public function getIdayuda()
    {
        return $this->idayuda;
    }
}

