<?php

namespace tutoriasBundle\Entity;

/**
 * PerfilFuncion
 */
class PerfilFuncion
{
    /**
     * @var integer
     */
    private $idperfil;

    /**
     * @var integer
     */
    private $idfuncion;

    /**
     * @var integer
     */
    private $idperfilFuncion;


    /**
     * Set idperfil
     *
     * @param integer $idperfil
     *
     * @return PerfilFuncion
     */
    public function setIdperfil($idperfil)
    {
        $this->idperfil = $idperfil;

        return $this;
    }

    /**
     * Get idperfil
     *
     * @return integer
     */
    public function getIdperfil()
    {
        return $this->idperfil;
    }

    /**
     * Set idfuncion
     *
     * @param integer $idfuncion
     *
     * @return PerfilFuncion
     */
    public function setIdfuncion($idfuncion)
    {
        $this->idfuncion = $idfuncion;

        return $this;
    }

    /**
     * Get idfuncion
     *
     * @return integer
     */
    public function getIdfuncion()
    {
        return $this->idfuncion;
    }

    /**
     * Get idperfilFuncion
     *
     * @return integer
     */
    public function getIdperfilFuncion()
    {
        return $this->idperfilFuncion;
    }
}

