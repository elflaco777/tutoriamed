<?php

namespace tutoriasBundle\Entity;

/**
 * Deporte
 */
class Deporte
{
    /**
     * @var string
     */
    private $decripcion;

    /**
     * @var integer
     */
    private $iddeporte;


    /**
     * Set decripcion
     *
     * @param string $decripcion
     *
     * @return Deporte
     */
    public function setDescripcion($decripcion)
    {
        $this->decripcion = $decripcion;

        return $this;
    }

    /**
     * Get decripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->decripcion;
    }

    /**
     * Get iddeporte
     *
     * @return integer
     */
    public function getIddeporte()
    {
        return $this->iddeporte;
    }
}

