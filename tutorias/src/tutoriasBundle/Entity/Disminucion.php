<?php

namespace tutoriasBundle\Entity;

/**
 * Disminucion
 */
class Disminucion
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $iddisminucion;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Disminucion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get iddisminucion
     *
     * @return integer
     */
    public function getIddisminucion()
    {
        return $this->iddisminucion;
    }
}

