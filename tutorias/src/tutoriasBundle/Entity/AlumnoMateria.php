<?php

namespace tutoriasBundle\Entity;

/**
 * AlumnoMateria
 */
class AlumnoMateria
{
    /**
     * @var integer
     */
    private $idmateria;

    /**
     * @var integer
     */
    private $idalumno;

    /**
     * @var \DateTime
     */
    private $horaentrada;

    /**
     * @var \DateTime
     */
    private $horasalida;

    /**
     * @var integer
     */
    private $dia;

    /**
     * @var integer
     */
    private $idalumnoMateria;


    /**
     * Set idmateria
     *
     * @param integer $idmateria
     *
     * @return AlumnoMateria
     */
    public function setIdmateria($idmateria)
    {
        $this->idmateria = $idmateria;

        return $this;
    }

    /**
     * Get idmateria
     *
     * @return integer
     */
    public function getIdmateria()
    {
        return $this->idmateria;
    }

    /**
     * Set idalumno
     *
     * @param integer $idalumno
     *
     * @return AlumnoMateria
     */
    public function setIdalumno($idalumno)
    {
        $this->idalumno = $idalumno;

        return $this;
    }

    /**
     * Get idalumno
     *
     * @return integer
     */
    public function getIdalumno()
    {
        return $this->idalumno;
    }

    /**
     * Set horaentrada
     *
     * @param \DateTime $horaentrada
     *
     * @return AlumnoMateria
     */
    public function setHoraentrada($horaentrada)
    {
        $this->horaentrada = $horaentrada;

        return $this;
    }

    /**
     * Get horaentrada
     *
     * @return \DateTime
     */
    public function getHoraentrada()
    {
        return $this->horaentrada;
    }

    /**
     * Set horasalida
     *
     * @param \DateTime $horasalida
     *
     * @return AlumnoMateria
     */
    public function setHorasalida($horasalida)
    {
        $this->horasalida = $horasalida;

        return $this;
    }

    /**
     * Get horasalida
     *
     * @return \DateTime
     */
    public function getHorasalida()
    {
        return $this->horasalida;
    }

    /**
     * Set dia
     *
     * @param integer $dia
     *
     * @return AlumnoMateria
     */
    public function setDia($dia)
    {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia
     *
     * @return integer
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * Get idalumnoMateria
     *
     * @return integer
     */
    public function getIdalumnoMateria()
    {
        return $this->idalumnoMateria;
    }
}

